package pl.pmajewski.dropgame;

import com.badlogic.gdx.Game;

import pl.pmajewski.dropgame.screen.PlayScreen;
import pl.pmajewski.dropgame.utils.GameManager;

public class GameMain extends Game {

	@Override
	public void create () {
		System.out.println("GameMain -> create");
		GameManager.getInstance().setGameInstance(this);
		GameManager.getInstance().getGameInstance().setScreen(new PlayScreen());
	}

	@Override
	public void dispose() {
	}
}
