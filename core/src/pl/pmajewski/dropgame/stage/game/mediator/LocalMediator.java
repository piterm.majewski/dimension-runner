package pl.pmajewski.dropgame.stage.game.mediator;

import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;

import pl.pmajewski.dropgame.stage.game.mediator.dto.Message;


public class LocalMediator implements Mediator {

    private static LocalMediator instance;

    private HashMap<Class<? extends Message>, List<Colleague>> colleagues = new HashMap<>();

    private LocalMediator() { }

    @Override
    public <T extends Message> void register(Colleague colleague, Class<T> clazz) {
        List<Colleague> participants = colleagues.get(clazz);
        if (participants == null) {
            participants = new LinkedList<>();
            colleagues.put(clazz, participants);
        }

        if(!participants.contains(colleague)) {
            participants.add(colleague);
        }
    }

    @Override
    public <T extends Message> void register(Colleague colleague, Class<T>... clazz) {
        for (Class<T> temp: clazz) {
            register(colleague, temp);
        }
    }

    @Override
    public <T extends Message> void emit(T message) {
        List<Colleague> participants = this.colleagues.get(message.getClass());
        if (participants != null) {
            emitMessage(participants, message);
        }
    }

    private <T extends Message> void emitMessage(List<Colleague> participants, T message) {
        for (Colleague temp: participants) {
            temp.notify(message);
        }
    }

    public static Mediator getInstance() {
        if(instance == null) {
            synchronized (LocalMediator.class) {
                if(instance == null) {
                    instance = new LocalMediator();
                }
            }
        }

        return instance;
    }
}
