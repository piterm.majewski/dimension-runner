package pl.pmajewski.dropgame.model;

import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.Batch;
import com.badlogic.gdx.graphics.g2d.TextureRegion;
import com.badlogic.gdx.math.Vector2;
import com.badlogic.gdx.physics.box2d.Body;
import com.badlogic.gdx.physics.box2d.BodyDef;
import com.badlogic.gdx.physics.box2d.CircleShape;
import com.badlogic.gdx.physics.box2d.Fixture;
import com.badlogic.gdx.physics.box2d.FixtureDef;
import com.badlogic.gdx.physics.box2d.World;

import java.util.LinkedList;
import java.util.List;

import pl.pmajewski.dropgame.utils.DebugLogger;
import pl.pmajewski.dropgame.utils.GameManager;
import pl.pmajewski.dropgame.utils.GlobalProperties;

public class PlayerModel extends GameActor implements PositionSource {

    enum RunPosition {TOP, DOWN};

    private TextureRegion characterTexture;
    private List<PositionListener> positionListeners = new LinkedList<>();
    private RunPosition runPosition;

    public PlayerModel(World world) {
        super(world);
        create(world);
        setUpTexture();
        this.runPosition = RunPosition.DOWN;
    }

    public PlayerModel(World world, PositionListener positionListener) {
        this(world);
        addPositionListener(positionListener);
    }

    @Override
    public Float getBodyWidth() {
        return GlobalProperties.PLAYER_WIDTH;
    }

    @Override
    public Float getBodyHeight() {
        return GlobalProperties.PLAYER_HEIGHT;
    }

    @Override
    public void act(float delta) {
        super.act(delta);
        notifyListeners(body.getPosition());
        DebugLogger.debug(500l,"bodyPosition="+body.getPosition()+" linearVelocity="+body.getLinearVelocity()+" angle="+body.getAngle(), "PLAYER");
    }

    @Override
    public void draw(Batch batch, float parentAlpha) {
        super.draw(batch, parentAlpha);
        drawHelper(batch, characterTexture);
    }

    private void setUpTexture() {
        Texture texture = GameManager.getInstance().getAssetManager()
                .get(GlobalProperties.PLAYER_TEXTURE, Texture.class);
        this.characterTexture = new TextureRegion(texture);
    }

    private void create(World world) {
        BodyDef bodyDef = new BodyDef();
        bodyDef.type = BodyDef.BodyType.DynamicBody;
        bodyDef.position.set(new Vector2(GlobalProperties.PLAYER_X, GlobalProperties.PLAYER_DOWN_Y));


        CircleShape shape = new CircleShape();
        shape.setRadius(GlobalProperties.PLAYER_WIDTH / 2);

        FixtureDef fixtureDef = new FixtureDef();
        fixtureDef.shape = shape;
        fixtureDef.density = GlobalProperties.PLAYER_DENSITY;
        fixtureDef.restitution = 0f;
        fixtureDef.friction = 0f;

        Body body = world.createBody(bodyDef);
        body.setLinearVelocity(new Vector2(GlobalProperties.PLAYER_VELOCITY_X, GlobalProperties.PLAYER_VELOCITY_Y));
        body.setGravityScale(GlobalProperties.PLAYER_GRAVITY_SCALE);
        Fixture fixture = body.createFixture(fixtureDef);
        fixture.setUserData(this);
        body.setUserData(this);
        body.resetMassData();

        shape.dispose();
        this.body = body;
    }

    public void addPositionListener(PositionListener listener) {
        positionListeners.add(listener);
    }

    private void notifyListeners(Vector2 position) {
        for (PositionListener listener: positionListeners) {
            listener.notify(position);
        }
    }

    public void reflect() {
        DebugLogger.debug("", "PLAYER", "REFLECT");
        switch (runPosition) {
            case TOP:
                this.body.setTransform(getBodyPosition().x, GlobalProperties.PLAYER_DOWN_Y, 0);
                DebugLogger.debug("position=" + getBodyPosition(), "PLAYER", "REFLECT", "TOP");
                break;
            case DOWN:
                this.body.setTransform(getBodyPosition().x, GlobalProperties.PLAYER_TOP_Y, 0);
                DebugLogger.debug("position=" + getBodyPosition(), "PLAYER", "REFLECT", "DOWN");
                break;
        }
        reflectRunPosition();
    }

    private void reflectRunPosition() {
        if(runPosition.equals(RunPosition.DOWN)) {
            runPosition = RunPosition.TOP;
        } else {
            runPosition = RunPosition.DOWN;
        }
    }
}
