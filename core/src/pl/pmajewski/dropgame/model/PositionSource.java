package pl.pmajewski.dropgame.model;

public interface PositionSource {

    void addPositionListener(PositionListener listener);
}
